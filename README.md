# CACAO Hello World CLI

This repository is used as a Hello World example for CACAO using Argo Workflows. It demonstrates an Argo Workflow with CACAO as well as a command-line based application being displayed on HTTP using a sidecar container

After running, you can see the Workflow output using curl or a web browser
