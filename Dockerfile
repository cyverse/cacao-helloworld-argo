FROM alpine
RUN apk add --no-cache perl
COPY cowsay /usr/local/bin/cowsay
RUN chmod +x /usr/local/bin/cowsay
ARG filename=swamp_land.cow
COPY ./$filename /usr/local/share/cows/default.cow
ENTRYPOINT ["/usr/local/bin/cowsay"]
